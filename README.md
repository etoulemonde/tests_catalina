# Introduction

This an example of a simple machine learning project based on data from Engie.

Data can be downloaded [here](https://opendata-renewables.engie.com/media/datasets/01c55756-5cd6-4f60-9f63-2d771bb25a1a.zip)

or with this command line 
```commandline
	curl -L https://opendata-renewables.engie.com/media/datasets/01c55756-5cd6-4f60-9f63-2d771bb25a1a.zip \
		-o data/la-haute-borne-data-2017-2020.zip
	unzip data/la-haute-borne-data-2017-2020.zip -d data/
	rm data/la-haute-borne-data-2017-2020.zip
```

# Setup 

To install dependencies 
```commandline
pip install -r requirements.txt
pip install -r requirement_dev.txt
```

To run existing tests :
```commandline
python -m pytest
```

To run tests with code coverage
```commandline
coverage run -m pytest
coverage html
```
